import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  final imageList = [
    'https://cdn.pixabay.com/photo/2016/03/05/19/02/hamburger-1238246__480.jpg',
    'https://cdn.pixabay.com/photo/2016/11/20/09/06/bowl-1842294__480.jpg',
    'https://cdn.pixabay.com/photo/2017/01/03/11/33/pizza-1949183__480.jpg',
    'https://cdn.pixabay.com/photo/2017/02/03/03/54/burger-2034433__480.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CirclePageIndicator Demo'),
      ),
      body: Swiper(
          //layout: SwiperLayout.CUSTOM,
          // customLayoutOption: new CustomLayoutOption(startIndex: -1, stateCount: 3)
          //     .addRotate([0.0 / 180, 0.0, 0.0 / 180]).addTranslate([
          //   new Offset(-310.0, 0.0),
          //   new Offset(0.0, 0.0),
          //   new Offset(310.0, 0.0) //for right element
          // ]),
          itemWidth: 150,
          itemHeight: 100,
          itemBuilder: (context, index) {
            return Image.network(
              imageList[index],
              fit: BoxFit.cover,
            );
          },
          itemCount: imageList.length),
    );
  }
}
