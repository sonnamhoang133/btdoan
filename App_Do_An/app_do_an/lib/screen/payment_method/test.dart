import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class RestaurantDetail extends StatefulWidget {
  @override
  _RestaurantDetailState createState() => _RestaurantDetailState();
}

class _RestaurantDetailState extends State<RestaurantDetail> {
  List listImg = [
    'https://www.chapter3d.com/wp-content/uploads/2020/08/anh-chan-dung.jpg',
    'https://www.chapter3d.com/wp-content/uploads/2020/08/anh-chan-dung.jpg',
    'https://www.chapter3d.com/wp-content/uploads/2020/08/anh-chan-dung.jpg'
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: 230,
          child: Swiper(
            itemBuilder: (BuildContext context, int index) {
              return Container(
                padding: EdgeInsets.symmetric(vertical: 3),
                margin: EdgeInsets.symmetric(horizontal: 2),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/images/loader_img.gif',
                    image: listImg[index] == null ? '' : listImg[index],
                    fit: BoxFit.cover,
                  ),
                ),
              );
            },
            itemCount: listImg.length,
            outer: false,
            autoplay: true,
            // pagination: SwiperPagination(
            //   alignment: Alignment.bottomCenter,
            //   builder: DotSwiperPaginationBuilder(
            //     color: Colors.grey.shade300,
            //     activeColor: CommonStyle.colorBlueLight,
            //     activeSize: 8,
            //     size: 5,
            //   ),
            // ),
          ),
        ),
      ),
    );
  }
}
