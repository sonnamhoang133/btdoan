import 'package:app_do_an/style/common_styles.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CarouseSlider extends StatefulWidget {
  @override
  _CarouseSliderState createState() => _CarouseSliderState();
}

class _CarouseSliderState extends State<CarouseSlider> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        primary: false,
        children: [
          CarouselSlider(
            items: [
              buildContainer(
                image: 'assets/images/image.png',
                text1: 'McDonald',
                svg: 'assets/icons/star.svg',
                text2: '4.1',
              ),
              buildContainer(
                image: 'assets/images/image2.png',
                text1: 'Lucky',
                svg: 'assets/icons/star.svg',
                text2: '4.1',
              ),
              //2nd Image of Slider
            ],
            //Slider Container properties
            options: CarouselOptions(
              height: 179.8,
              initialPage: 0,
              viewportFraction: 0.5,
              // enlargeCenterPage: true,
              autoPlay: true,
              // aspectRatio: 16 / 9,
              autoPlayInterval: Duration(seconds: 2),
              //autoPlayCurve: Curves.fastOutSlowIn,
              enableInfiniteScroll: true,
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              scrollDirection: Axis.horizontal,
            ),
          ),
        ],
      ),
    );
  }

  Container buildContainer({
    @required String image,
    String text1,
    String svg,
    String text2,
  }) {
    return Container(
      height: 179.8,
      width: 176,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(image),
          Row(
            children: [
              Text(
                text1,
                style: CommonStyle.textSize_18,
              ),
              Spacer(),
              SvgPicture.asset(svg),
              Text(
                text2,
                style: CommonStyle.textSize_12,
              )
            ],
          ),
          Spacer(),
          Row(
            children: [
              Text(
                '40-50 mins ',
                style: CommonStyle.textSize_14,
              ),
              Text(
                '•  €30.0 for two',
                style: CommonStyle.textSize_14,
              )
            ],
          )
        ],
      ),
    );
  }
}
