import 'package:app_do_an/screen/couponScreen/coupon_applied_screen.dart';
import 'package:app_do_an/screen/widget/carousel_slider2.dart';
import 'package:app_do_an/style/common_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: CommonStyle.colorGrey.withOpacity(1),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 16, left: 16),
                        height: 46,
                        width: 46,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(6),
                          ),
                          color: Colors.grey,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 1,
                            )
                          ],
                        ),
                        child: Row(
                          children: [
                            Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(6),
                                  ),
                                  color: CommonStyle.colorWhite),
                              child: Center(
                                child: SvgPicture.asset(
                                  'assets/icons/location.svg',
                                  height: 24,
                                  width: 24,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 7,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 16,
                        ),
                        height: 46,
                        width: 300,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(6),
                          ),
                          color: Colors.grey,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 1,
                            )
                          ],
                        ),
                        child: Row(
                          children: [
                            Container(
                              height: 45,
                              width: 299,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(6),
                                  ),
                                  color: CommonStyle.colorWhite),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: Row(
                          children: [
                            Text(
                              'Top Categories',
                              style: CommonStyle.textBoldSize_24.copyWith(
                                  color: CommonStyle.colorBlackText, fontWeight: FontWeight.w400),
                            ),
                            Spacer(),
                            SvgPicture.asset('assets/icons/Filter.svg'),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "Filter",
                              style: CommonStyle.textSize_16,
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 100,
                        child: CarouselSlider2(),
                      ),
                    ],
                  ),
                  Container(
                    height: 1,
                    width: MediaQuery.of(context).size.width,
                    color: CommonStyle.colorWhite,
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: Row(
                          children: [
                            Text(
                              'Popular Items',
                              style: CommonStyle.textBoldSize_24.copyWith(
                                  color: CommonStyle.colorBlackText, fontWeight: FontWeight.w400),
                            ),
                            Spacer(),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "View all",
                              style: CommonStyle.textSize_16,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.08,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20),
                    ),
                    color: Colors.red,
                  ),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CouponScreen()),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
