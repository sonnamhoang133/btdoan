import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:app_do_an/screen/home_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: AnimatedSplashScreen(
        splash: Image.asset('assets/images/logo.png'),
        nextScreen: HomeScreen(),
        splashTransition: SplashTransition.sizeTransition,
        backgroundColor: Colors.red,
        splashIconSize: 100,
        duration: 1,
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
