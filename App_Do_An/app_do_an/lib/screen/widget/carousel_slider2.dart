import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CarouselSlider2 extends StatefulWidget {
  @override
  _CarouselSlider2State createState() => _CarouselSlider2State();
}

class _CarouselSlider2State extends State<CarouselSlider2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        primary: false,
        children: [
          CarouselSlider(
            items: [
              buildContainer(
                image: 'assets/images/image8.png',
                text1: 'Pizza',
                text2: 'Desserts',
              ),
              buildContainer(
                image: 'assets/images/image9.png',
                text1: 'Pizza',
                text2: 'Desserts',
              ),
              buildContainer(
                image: 'assets/images/Image10.png',
                text1: 'Pizza',
                text2: 'Desserts',
              ),
              buildContainer(
                image: 'assets/images/Image11.png',
                text1: 'Pizza',
                text2: 'Desserts',
              ),
            ],
            options: CarouselOptions(
              height: 90,
              initialPage: 0,
              viewportFraction: 0.26,
              // enlargeCenterPage: true,
              autoPlay: true,
              // aspectRatio: 16 / 9,
              autoPlayInterval: Duration(seconds: 2),
              //autoPlayCurve: Curves.fastOutSlowIn,
              enableInfiniteScroll: true,
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              scrollDirection: Axis.horizontal,
            ),
          )
        ],
      ),
    );
  }

  Container buildContainer({@required String image, String text1, String text2}) {
    return Container(
      height: 90.5,
      width: 88,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        image: DecorationImage(
          image: AssetImage(image),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(text1),
        ],
      ),
    );
  }
}
