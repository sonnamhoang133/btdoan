import 'package:app_do_an/screen/widget/carousel_slider_restaurant.dart';
import 'package:app_do_an/screen/widget/custom_all_offer.dart';
import 'package:app_do_an/screen/widget/custom_listview.dart';
import 'package:app_do_an/screen/widget/today_offer_carousel_slider.dart';
import 'package:app_do_an/style/common_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomTabBar extends StatefulWidget {
  @override
  _CustomTabBarState createState() => _CustomTabBarState();
}

class _CustomTabBarState extends State<CustomTabBar> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: DefaultTabController(
            length: 2,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 24, left: 34, right: 34),
                  child: Container(
                    height: 38,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8.91),
                      border: Border.all(
                        width: 1,
                        color: Colors.red,
                      ),
                    ),
                    child: TabBar(
                      //labelColor: index == 0 ? Colors.white : Colors.green,
                      labelStyle: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 15,
                      ),
                      unselectedLabelColor: Colors.red,
                      unselectedLabelStyle: TextStyle(fontSize: 14),
                      //indicatorColor: Colors.white,
                      indicator: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      tabs: [
                        Tab(
                          text: 'Unternehmen',
                        ),
                        Tab(
                          text: 'Privatperson',
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.symmetric(
                    vertical: 10,
                  ),
                  child: TabBarView(
                    children: [
                      SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 130,
                                width: MediaQuery.of(context).size.width * 1,
                                child: CarouselSliderRestaurant(),
                              ),
                              SizedBox(
                                height: 28,
                              ),
                              Text(
                                'Today Offer',
                                style: CommonStyle.textSize_20,
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Container(
                                height: 180,
                                child: CarouseSlider(),
                              ),
                              // Container(
                              //   height: 121,
                              //   child: CarouseSlider2(),
                              // ),
                              SizedBox(
                                height: 36.2,
                              ),
                              SizedBox(
                                height: 30,
                                child: Row(
                                  children: [
                                    Text(
                                      'Free Delivery *',
                                      style: CommonStyle.textSize_20,
                                    ),
                                    Spacer(),
                                    Container(
                                      height: 29,
                                      width: 71,
                                      decoration: BoxDecoration(
                                        color: CommonStyle.colorGrey,
                                        borderRadius: BorderRadius.circular(6),
                                      ),
                                      child: Center(
                                        child: Text(
                                          'View all',
                                          style: CommonStyle.textSize_14,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Container(
                                height: 250,
                                child: CustomListView(),
                              ),
                              Container(
                                height: 121,
                                child: CustonAllOfer(),
                              ),
                              Container(
                                height: 100,
                                color: CommonStyle.redColor,
                              ),
                              Container(
                                height: 100,
                                color: CommonStyle.colorPink,
                              ),
                              Container(
                                height: 100,
                                color: CommonStyle.redColor,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height,
                        color: CommonStyle.colorPink,
                        child: SingleChildScrollView(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
