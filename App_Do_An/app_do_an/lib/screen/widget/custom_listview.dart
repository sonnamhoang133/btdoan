import 'package:app_do_an/style/common_styles.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        primary: false,
        children: [
          CarouselSlider(
            options: CarouselOptions(
              scrollDirection: Axis.horizontal,
              height: 120,
              initialPage: 0,
              viewportFraction: 0.77,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 2),
              enableInfiniteScroll: true,
              autoPlayAnimationDuration: Duration(milliseconds: 800),
            ),
            items: [
              buildContainer(
                  image: 'assets/images/Image4.png', text1: 'Monginis Cake', text2: 'Desserts'),
              buildContainer(
                  image: 'assets/images/Image3.png', text1: 'Monginis Cake', text2: 'Desserts'),
              // StaggeredGridView.countBuilder(
              //   scrollDirection: Axis.vertical,
              //   crossAxisCount: 1,
              //   itemCount: 1,
              //   itemBuilder: (BuildContext context, int indext) => buildContainer(
              //       image: 'assets/images/Image4.png', text1: 'adssa', text2: 'qwerrr'),
              //   staggeredTileBuilder: (int index) => StaggeredTile.count(1, 0.44),
              //   mainAxisSpacing: 2,
              //   crossAxisSpacing: 6,
              // ),
            ],
          ),
          SizedBox(
            height: 16,
          ),
          CarouselSlider(
            options: CarouselOptions(
              height: 120,
              initialPage: 0,
              viewportFraction: 0.77,
              // enlargeCenterPage: true,
              autoPlay: true,
              // aspectRatio: 16 / 9,
              autoPlayInterval: Duration(seconds: 2),
              //autoPlayCurve: Curves.fastOutSlowIn,
              enableInfiniteScroll: true,
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              scrollDirection: Axis.horizontal,
            ),
            items: [
              buildContainer(
                  image: 'assets/images/image5.png', text1: 'Biriyani', text2: 'Rice meals'),
              buildContainer(
                  image: 'assets/images/image6.png', text1: 'Monginis Cake', text2: 'Desserts'),
              // StaggeredGridView.countBuilder(
              //   scrollDirection: Axis.vertical,
              //   crossAxisCount: 1,
              //   itemCount: 1,
              //   itemBuilder: (BuildContext context, int indext) => buildContainer(
              //       image: 'assets/images/Image4.png', text1: 'adssa', text2: 'qwerrr'),
              //   staggeredTileBuilder: (int index) => StaggeredTile.count(1, 0.44),
              //   mainAxisSpacing: 2,
              //   crossAxisSpacing: 6,
              // ),
            ],
          ),
        ],
      ),
    );
  }

  Container buildContainer({@required String image, String text1, String text2}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      height: 200,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          border: Border.all(color: CommonStyle.colorGrey, width: 1)),
      child: Padding(
        padding: const EdgeInsets.only(top: 7, left: 10, bottom: 13),
        child: Row(
          children: [
            Image.asset(image),
            SizedBox(
              width: 16,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  text1,
                  style: CommonStyle.textSize_18,
                ),
                Text(
                  text2,
                  style: CommonStyle.textSize_14,
                ),
                SizedBox(
                  height: 13.4,
                ),
                Container(
                  height: 1,
                  width: 50,
                  color: CommonStyle.colorGrey,
                ),
                SizedBox(
                  height: 4,
                ),
                Row(
                  children: [
                    SvgPicture.asset('assets/icons/star.svg'),
                    Text(
                      '4.1  •  40-50 mins',
                      style: CommonStyle.textSize_12,
                    )
                  ],
                ),
                SizedBox(
                  height: 6.4,
                ),
                Text(
                  '€ 14.20',
                  style: CommonStyle.textSize_18,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
