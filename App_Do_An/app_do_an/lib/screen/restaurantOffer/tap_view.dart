import 'package:app_do_an/screen/restaurantOffer/widget/promos.dart';
import 'package:app_do_an/screen/restaurantOffer/widget/restaurant.dart';
import 'package:app_do_an/style/common_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TabView extends StatefulWidget {
  @override
  _TabViewState createState() => _TabViewState();
}

class _TabViewState extends State<TabView> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 24, left: 34, right: 34),
              child: Align(
                alignment: Alignment.center,
                child: Container(
                  width: 307,
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            index = 0;
                          });
                        },
                        child: Container(
                          height: 38,
                          width: 152,
                          decoration: BoxDecoration(
                            color: index == 0 ? CommonStyle.redColor : CommonStyle.colorWhite,
                            borderRadius: BorderRadius.circular(6),
                            border: Border.all(color: CommonStyle.redColor, width: 0.5),
                          ),
                          child: Center(
                            child: Text(
                              "RESTAURANTS",
                              style: CommonStyle.textSize_20.copyWith(
                                  color:
                                      index == 0 ? CommonStyle.colorWhite : CommonStyle.redColor),
                            ),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            index = 1;
                          });
                        },
                        child: Container(
                          height: 38,
                          width: 152,
                          decoration: BoxDecoration(
                            color: index == 1 ? CommonStyle.redColor : CommonStyle.colorWhite,
                            borderRadius: BorderRadius.circular(6),
                            border: Border.all(color: CommonStyle.redColor, width: 0.5),
                          ),
                          child: Center(
                            child: Text(
                              "PROMOS",
                              style: CommonStyle.textSize_20.copyWith(
                                  color:
                                      index == 1 ? CommonStyle.colorWhite : CommonStyle.redColor),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            index == 0 ? Restaurant() : Promos(),
          ],
        ),
      ),
    );
  }
}
