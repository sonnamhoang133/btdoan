import 'package:flutter/cupertino.dart';

abstract class CommonStyle {
  static Color colorWhite = const Color(0xFFFFFFFF);
  static Color colorSelectItemNavigation = const Color(0xff16B9E9);
  static Color colorNavigationBottom = const Color(0xffB8BAC9);
  static Color colorGrey = Color(0xffE5E5E5);
  static Color colorGrey2 = Color(0xffE6E6E6);
  static Color colorBlue = Color(0xffB5DAFF);
  static Color colorBackground = Color(0xffF1F1F1);
  static Color colorGreyText = Color(0xff6E7985);
  static Color colorBlackText = Color(0xff253647);
  static Color colorPink = Color(0xffFF77D6);
  static Color redColor = Color(0xFFD62C2C);
  static Color grayBackground = Color(0xFFF3F3F3);
  static Color colorRed = const Color(0xFFFE4234);
  static Color darkBlueColor = Color(0xFF0045BB);
  static Color colorBlueLight = Color(0xFF39adf0);
  static Color colorBlueButton = Color(0xFF16B9E9);
  static Color colorBlueFile = Color(0xFF81AFD6);
  static Color colorBlueDar = Color(0xFF12128A);
  static Color colorBoxShadow = Color(0xFFDDE5F2);
  static Color colorHintText = Color(0xFFADADAD);
  static Color colorLightBlue = const Color(0xFF16B9E9); //for button
  static Color colorTextBlueDar = const Color(0xFF1469BB); //for button
  static Color colorBlueNotification = const Color(0xFFe7f3ff); //for button
  static Color colorBlueAppBar = const Color(0xFF157FC8); //for button
  static Color colorButtonLeft = const Color(0xFFFD3207); //for button
  static Color colorButtonRight = const Color(0xFFFD7C04); //for button
  static Color colorButtonRight1 = const Color(0xFF02599E); //for button
  static Color colorButtonLeft1 = const Color(0xFF40B7F9); //for button
  static Color colorCam = const Color(0xffD44843);
  static Color greenColor = const Color(0xff18E637); //for button

  static List validEmail = ['test@gmail.com'];
  static BoxShadow defaultShadow = BoxShadow(
    color: CommonStyle.colorBoxShadow,
    spreadRadius: 0,
    blurRadius: 20,
    offset: Offset(0, 5), // changes position of shadow
  );
  static TextStyle textSize_10 = TextStyle(
    fontSize: 10,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.w300,
  );
  static TextStyle textSize_12 = TextStyle(
    fontSize: 12,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.w300,
  );
  static TextStyle textSize_14 = TextStyle(
    fontSize: 14,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.w300,
  );
  static TextStyle textSize_16 = TextStyle(
    fontSize: 16,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.w300,
  );
  static TextStyle textSize_18 = TextStyle(
    fontSize: 18,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.w300,
  );
  static TextStyle textSize_20 = TextStyle(
    fontSize: 20,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.w300,
  );
  static TextStyle textSize_22 = TextStyle(
    fontSize: 22,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.w300,
  );
  static TextStyle textSize_24 = TextStyle(
    fontSize: 24,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.w300,
  );
  static TextStyle textSize_26 = TextStyle(
    fontSize: 26,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.w300,
  );
  static TextStyle textSize_36 = TextStyle(
    fontSize: 36,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.w300,
  );
  static TextStyle textBoldSize_10 = TextStyle(
    fontSize: 10,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.bold,
  );
  static TextStyle textBoldSize_12 = TextStyle(
    fontSize: 12,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.bold,
  );
  static TextStyle textBoldSize_14 = TextStyle(
    fontSize: 14,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.bold,
  );
  static TextStyle textBoldSize_16 = TextStyle(
    fontSize: 16,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.bold,
  );
  static TextStyle textBoldSize_18 = TextStyle(
    fontSize: 18,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.bold,
  );
  static TextStyle textBoldSize_20 = TextStyle(
    fontSize: 20,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.bold,
  );
  static TextStyle textBoldSize_22 = TextStyle(
    fontSize: 22,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.bold,
  );
  static TextStyle textBoldSize_24 = TextStyle(
    fontSize: 24,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.bold,
  );
  static TextStyle textBoldSize_26 = TextStyle(
    fontSize: 26,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.bold,
  );
  static TextStyle textBoldSize_36 = TextStyle(
    fontSize: 36,
    color: CommonStyle.colorBlackText,
    fontWeight: FontWeight.bold,
  );
}
