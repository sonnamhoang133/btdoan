import 'package:app_do_an/style/common_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Payment1 extends StatefulWidget {
  @override
  _Payment1State createState() => _Payment1State();
}

class _Payment1State extends State<Payment1> {
  int countradio = 0;
  void _handleRadioValueChange(int value) {
    setState(() {
      countradio = value;
      switch (countradio) {
        case 0:
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CommonStyle.colorGrey2,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 375,
                decoration: BoxDecoration(
                  color: CommonStyle.colorWhite,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20),
                  ),
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 13, left: 16),
                      child: Row(
                        children: [
                          Container(
                            height: 45,
                            width: 45,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              color: CommonStyle.colorGrey2,
                            ),
                            child: Center(
                              child: SvgPicture.asset('assets/icons/back.svg'),
                            ),
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          SizedBox(
                            height: 46,
                            width: 152,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Payment Options',
                                  style:
                                      CommonStyle.textSize_18.copyWith(fontWeight: FontWeight.w400),
                                ),
                                Text(
                                  '1 item(s), To pay: ^27.27',
                                  style: CommonStyle.textSize_14.copyWith(
                                      color: CommonStyle.colorBlackText,
                                      fontWeight: FontWeight.w300),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 28, left: 16, right: 21),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Wallets',
                                style: CommonStyle.textSize_24,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          buildRow(
                              svg: 'assets/icons/amazon.svg',
                              text1: 'Amazon Pay',
                              text2: 'LINK ACCOUNT'),
                          SizedBox(
                            height: 16,
                          ),
                          buildRow(
                              svg: 'assets/icons/paytm.svg', text1: 'Paytm', text2: 'LINK ACCOUNT'),
                          SizedBox(
                            height: 16,
                          ),
                          buildRow(
                              svg: 'assets/icons/paypal.svg',
                              text1: 'PayPal',
                              text2: 'LINK ACCOUNT'),
                          SizedBox(
                            height: 16,
                          ),
                          buildRow(
                              svg: 'assets/icons/google pay.svg',
                              text1: 'Google Pay',
                              text2: 'LINK ACCOUNT'),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 19,
              ),
              Container(
                height: 94,
                decoration: BoxDecoration(
                  color: CommonStyle.colorWhite,
                  borderRadius: BorderRadius.circular(16),
                ),
                child: Padding(
                  padding: EdgeInsets.only(top: 12, right: 16, left: 16, bottom: 20),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            'Credit/Debit Cards',
                            style: CommonStyle.textSize_20,
                          ),
                          Spacer(),
                          SvgPicture.asset('assets/icons/Polygon 1.svg')
                        ],
                      ),
                      SizedBox(
                        height: 17,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 28,
                          ),
                          Text('ADD NEW CARD'),
                          Spacer(),
                          SvgPicture.asset('assets/icons/Bitmap.svg')
                        ],
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 204,
                decoration: BoxDecoration(
                  color: CommonStyle.colorWhite,
                  borderRadius: BorderRadius.circular(16),
                ),
                child: Padding(
                  padding: EdgeInsets.only(left: 16, top: 8, right: 16, bottom: 25),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'Net Banking',
                            style: CommonStyle.textSize_20,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 23,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          buildColumn(svg: 'assets/icons/Standard Chartered.svg', text: 'SC'),
                          buildColumn(svg: 'assets/icons/Bank Bukopin.svg', text: 'BB'),
                          buildColumn(svg: 'assets/icons/Bank Indonesia.svg', text: 'BJB'),
                          buildColumn(svg: 'assets/icons/Bank Bukopin Copy.svg', text: 'CIMB'),
                          buildColumn(svg: 'assets/icons/HSBC.svg', text: 'HSBC'),
                        ],
                      ),
                      Divider(height: 11.7),
                      Expanded(
                        child: Row(
                          children: [
                            Text(
                              'MORE BANKS',
                              style: CommonStyle.textSize_16,
                            ),
                            Spacer(),
                            SvgPicture.asset('assets/icons/Path.svg')
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 13,
              ),
              Container(
                height: 150,
                decoration: BoxDecoration(
                  color: CommonStyle.colorWhite,
                  borderRadius: BorderRadius.circular(16),
                ),
                child: Padding(
                  padding: EdgeInsets.only(top: 12, left: 16, right: 18),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            'Pay On Delivery',
                            style: CommonStyle.textSize_20,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        children: [
                          Text(
                            'Cash ONLY',
                            style: CommonStyle.textSize_16,
                          ),
                          Spacer(),
                          Radio(
                            value: 0,
                            groupValue: countradio,
                            onChanged: _handleRadioValueChange,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Column buildColumn({@required String svg, String text}) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(color: CommonStyle.colorGrey2),
          ),
          child: SvgPicture.asset(svg),
          height: 50,
          width: 50,
        ),
        SizedBox(
          height: 7.8,
        ),
        Text(
          text,
          style: CommonStyle.textSize_18,
        )
      ],
    );
  }

  Row buildRow({@required String svg, String text1, String text2}) {
    return Row(
      children: [
        Container(
          height: 45,
          width: 45,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
          ),
          child: Center(
            child: SvgPicture.asset(svg),
          ),
        ),
        SizedBox(
          width: 8,
        ),
        Text(
          text1,
          style: CommonStyle.textSize_20.copyWith(fontWeight: FontWeight.w400),
        ),
        Spacer(),
        Text(
          text2,
          style: CommonStyle.textSize_18.copyWith(fontWeight: FontWeight.w400),
        ),
      ],
    );
  }
}
