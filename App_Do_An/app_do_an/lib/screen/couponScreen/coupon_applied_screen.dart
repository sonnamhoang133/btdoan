import 'package:app_do_an/screen/restaurantOffer/tap_view.dart';
import 'package:app_do_an/style/common_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CouponScreen extends StatefulWidget {
  @override
  _CouponScreenState createState() => _CouponScreenState();
}

class _CouponScreenState extends State<CouponScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CommonStyle.grayBackground,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 212,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15),
                  ),
                  color: CommonStyle.colorWhite,
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 16, right: 16, left: 16),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: CommonStyle.colorGrey2,
                              ),
                              child: Center(
                                child: SvgPicture.asset('assets/icons/back.svg'),
                              ),
                            ),
                            SizedBox(
                              width: 23,
                            ),
                            Text(
                              'Enter discount code',
                              style: CommonStyle.textSize_24
                                  .copyWith(color: CommonStyle.colorBlackText),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 13.7,
                      ),
                      Container(
                        height: 120,
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Enter discount code',
                              style: CommonStyle.textSize_16.copyWith(
                                  color: CommonStyle.colorBlackText, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: 45,
                              child: TextField(
                                obscureText: true,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: "Enter Code",
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 13,
                            ),
                            Expanded(
                              child: SizedBox(
                                height: 30,
                                width: MediaQuery.of(context).size.width,
                                child: FlatButton(
                                  child: Text(
                                    'APPLY NOW!',
                                    style: TextStyle(fontSize: 20.0),
                                  ),
                                  color: Colors.red,
                                  textColor: Colors.white,
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => TabView()),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              buildContainer(
                  image: 'assets/images/Paytm.png',
                  text1: "Get Unlimited free delivery using paytm",
                  text2: "Use code FREEDELPTM & get free delivery on all orders above Rs. 99",
                  text3:
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.",
                  text4: "CLOSE",
                  svg: 'assets/icons/up-arrow.svg'),
              buildContainer(
                  image: 'assets/images/Paytm.png',
                  text1: "adasdas",
                  text4: "EXPAND",
                  svg: 'assets/icons/down.svg'),
            ],
          ),
        ),
      ),
    );
  }

  Padding buildContainer(
      {@required String image,
      String text1,
      String text2,
      String text3,
      String text4,
      String svg}) {
    return Padding(
      padding: const EdgeInsets.only(top: 16, right: 16, left: 16),
      child: Container(
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(16), color: CommonStyle.colorWhite),
        child: Padding(
          padding: const EdgeInsets.only(top: 16, right: 16, left: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                child: Row(
                  children: [
                    Image.asset(image),
                    Spacer(),
                    Container(
                      height: 50,
                      width: 109,
                      decoration: BoxDecoration(
                        border: Border.all(color: CommonStyle.redColor, width: 0.5),
                      ),
                    ),
                  ],
                ),
                height: 50,
              ),
              SizedBox(
                height: 13.5,
              ),
              Text(
                text1,
                style: CommonStyle.textSize_14.copyWith(fontWeight: FontWeight.w400),
              ),
              SizedBox(
                height: 20,
              ),
              text2 == null
                  ? SizedBox()
                  : Text(
                      text2,
                      style: CommonStyle.textSize_12.copyWith(fontWeight: FontWeight.w300),
                    ),
              SizedBox(
                height: 20,
              ),
              text3 == null
                  ? SizedBox()
                  : Text(
                      text3,
                      style: CommonStyle.textSize_12.copyWith(fontWeight: FontWeight.w300),
                    ),
              SizedBox(
                height: 15,
              ),
              Row(
                children: [
                  Text(
                    text4,
                    style: CommonStyle.textBoldSize_16.copyWith(fontWeight: FontWeight.w400),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  SvgPicture.asset(
                    svg,
                    height: 20,
                    width: 20,
                    color: CommonStyle.colorBlackText.withOpacity(0.2),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
