import 'package:app_do_an/screen/widget/carousel_slider_restaurant.dart';
import 'package:flutter/widgets.dart';

class Restaurant extends StatefulWidget {
  @override
  _RestaurantState createState() => _RestaurantState();
}

class _RestaurantState extends State<Restaurant> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CarouselSliderRestaurant(),
      ],
    );
  }
}
