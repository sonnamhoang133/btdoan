import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';

class CarouselSliderRestaurant extends StatefulWidget {
  @override
  _CarouselSliderRestaurantState createState() => _CarouselSliderRestaurantState();
}

class _CarouselSliderRestaurantState extends State<CarouselSliderRestaurant> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          CarouselSlider(
            options: CarouselOptions(
              scrollDirection: Axis.horizontal,
              height: 120,
              autoPlay: true,
              aspectRatio: 16 / 9,
              autoPlayCurve: Curves.fastOutSlowIn,
              enableInfiniteScroll: true,
            ),
            items: [
              Container(
                height: 120,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  image: DecorationImage(
                    image: AssetImage('assets/images/Discount1.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  image: DecorationImage(
                    image: AssetImage('assets/images/Discount2.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
